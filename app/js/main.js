$(function () {
    var right_pos = 1000
    var right_pos2 = 1000
    var isMinus = false
    var cur_pos = 0
    let isScroll = false
    let cover = $(".words_cover_block")
    let imgs = $(".words_cover_block img")
    let middle = $(imgs[1])
    let ar_imgs1 = $(cover[0]).find("img").slice(0,3)
    let ar_imgs2 = $(cover[1]).find("img").slice(0,3)
    $('.advert__sliders').slick({
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight: true,
        slidesToShow: 3,
        prevArrow: "<button class='arrow-slider arrowLeft-slider'><img src='../img/arrow-left.svg'></button>",
        nextArrow: "<button class='arrow-slider arrowRight-slider'><img src='../img/arrow-right.svg'></button>",
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    let block = setInterval(function(e){
        $($(".words_cover_block")).css("right",right_pos + "px")
        $($(".words_cover_block")[1]).css("right",right_pos2 + "px")
        if(isMinus && isScroll){
            right_pos2 -= 13
            right_pos -= 8
        }
        else if(isScroll){
            right_pos2 += 13
            right_pos += 8
        }
        if(isMinus){
            right_pos2 -= 8
            right_pos -= 4 
        }
        else{
            right_pos2 += 8
            right_pos += 4 
        }
       
        isScroll = false
    },200);
    $(window).scroll(function(e){
        if(cur_pos < window.pageYOffset){
            isMinus = false
        }
        else{
            isMinus = true
        }
        cur_pos = this.window.pageYOffset
        isScroll = true
    })
});